# React-Todo-List-Express-Server
## Separate the Front-End and Back-End mini project.
### 自學將 todo-list 的前端與後端分開。
### 前端框架：React
### 後段框架：Node Express
### 資料庫：Firebase Firestore Database

### Getting Started

#### Prerequisites
該電腦需要有 Node 環境。

#### Installing

把專案 clone 到本地段
```
git clone https://gitlab.com/eugene2253902/react-todo-list-express-server.git
```

### Backend API Server Setup Reference
```
https://gitlab.com/eugene2253902/react-todo-list-express-server/-/tree/master/Firebase-Server
```

### React Todo-List Setup Reference
```
https://gitlab.com/eugene2253902/react-todo-list-express-server/-/tree/master/todo-list
```
